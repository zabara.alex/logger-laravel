<?php
namespace Leomax\Logger\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use packages\leomax\logger\src\Log;

class LogCustomServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $this->publishes([
//
//        ]);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(Log::class);
//        $this->app->bind(Lomax\Exception::class);
    }
}
