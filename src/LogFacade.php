<?php

namespace Leomax\Logger;

use Illuminate\Support\Facades\Facade;
use Leomax\ResovaApiClient;
use packages\leomax\logger\src\Log;

class LogFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return Log::class;
    }
}
