<?php

namespace Leomax\Logger\Methods;

use packages\leomax\logger\src\Singleton;
use packages\leomax\logger\src\Methods\MethodInterface;

class DatabaseClass extends Singleton implements MethodInterface
{
    public function writeLog($level, $message)
    {
        exit('Запись в базу данных');
    }
}
