<?php
namespace Leomax\Logger\Methods;

use packages\leomax\logger\src\Singleton;
use packages\leomax\logger\src\Methods\MethodInterface;

class TelegramClass extends Singleton implements MethodInterface
{
    protected $fileHandle;

    protected function __construct()
    {
//        $co url
    }

    protected function __destruct()
    {
        fclose($this->fileHandle);
    }

    public function writeLog($level, $message)
    {
        $date = date('Y-m-d G:i:s');
        $str = print_r($message, true);
        fwrite($this->fileHandle, "{$date} | {$level} | {$str} \n");
    }
}
