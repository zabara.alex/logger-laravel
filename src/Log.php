<?php

namespace Leomax\Logger;

use packages\leomax\logger\src\LoggerInterface;

/**
 * Class Logger
 */
class Log implements LoggerInterface
{
    private static $sentry = 'Sentry';
    private static function method($classFile)
    {
        $listenerClass = 'Leomax\\Logger\\Methods\\' . ucfirst($classFile) . 'Class';
        if (class_exists($listenerClass)) {
            return $listenerClass::getInstance();
        } else {
            exit("This class does not exist! $classFile \n $listenerClass");
        }
    }

    public static function emergency($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function alert($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function critical($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function error($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function warning($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function notice($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function info($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }

    public static function debug($context)
    {
        self::method(self::$sentry)->writeLog(__FUNCTION__, $context);
    }
}
