### Install component.
`composer require zabara_industry/logger`

### add to .env
`LeomaxLOGGER=file`

- `file` 
- `database`
- `ftp`

### Your file name
- `log.txt`
  
### You have method:
- `log()`

### Examples:

`use Leomax\Logger\Logger`

`Log::error('your text or array');`

If you need check your instance use

`Logger::getInstance();`